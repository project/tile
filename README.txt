The Tile module allows site administrators and themers to specify icons for use 
as tile on startscreen of windows 8.1.

Extra theme settings are provided so the tile can be configured for each theme, 
similar to the logo and shortcust icon settings provided by Drupal core.

Configuration

After enabling the module, you can customize your tile on the theme settings 
page(admin/appearance/settings).
